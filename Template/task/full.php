<?php
if ($task['description']=='')$task['description'] = 'No Description';
?>

<section class="tlf_task_content">
<?= $this->hook->render('template:task:show:top', array('task' => $task, 'project' => $project)) ?>
<?php if ($this->user->hasProjectAccess('TaskModificationController', 'edit', $task['project_id'])): ?>
    <?php if ($this->projectRole->canUpdateTask($task)): ?>
    	<div>
		<a href="<?=KANBOARD_URL?>?controller=TaskModificationController&amp;action=edit&amp;task_id=<?=$task['id']?>&amp;project_id=<?=$project['id']?>" class="js-modal-large" title="">
				<i class="fa fa-edit fa-fw js-modal-large" aria-hidden="true"></i>
		</a>
		</div>
    <?php endif ?>
<?php endif; ?>

<h2><?=$task['title']?></h2>

<?= $this->hook->render('template:task:show:before-description', array('task' => $task, 'project' => $project)) ?>
<article class="markdown">
    <?= $this->text->markdown($task['description'], isset($is_public) && $is_public) ?>
</article>

<?= $this->hook->render('template:task:show:before-subtasks', array('task' => $task, 'project' => $project)) ?>
<?php
if (count($subtasks)>0){
    echo '<h3>Subtasks</h3>';
}
?>
<?= $this->render('subtask/table', array(
    'subtasks' => $subtasks,
    'task' => $task,
    'editable' => $this->user->hasProjectAccess('SubtaskController', 'edit', $project['id'])
)) ?>

<?= $this->hook->render('template:task:show:before-internal-links', array('task' => $task, 'project' => $project)) ?>
<?php
if (count($internal_links)>0){
    echo '<h3>'.t('Internal References').'</h3>';
}
?>
<?= $this->render('task_internal_link/table', array(
    'links' => $internal_links,
    'task' => $task,
    'project' => $project,
    'editable' => $this->user->hasProjectAccess('TaskInternalLinkController', 'edit', $project['id']),
    'is_public' => false,
)) ?>

<?= $this->hook->render('template:task:show:before-external-links', array('task' => $task, 'project' => $project)) ?>
<?php
if (count($external_links)>0){
    echo '<h3>'.t('External References').'</h3>';
}
?>
<?= $this->render('task_external_link/table', array(
    'links' => $external_links,
    'task' => $task,
    'project' => $project,
)) ?>

<?= $this->hook->render('template:task:show:before-attachments', array('task' => $task, 'project' => $project)) ?>
<?php
if (count($images)>0){
    echo '<h3>'.t('Images').'</h3>';
    echo $this->render('task_file/images', array('task' => $task, 'images' => $images));
}
if (count($files)>0){
    echo '<h3>'.t('Files').'</h3>';
    echo $this->render('task_file/files', array('task' => $task, 'files' => $files));
}
?>

<?= $this->hook->render('template:task:show:before-comments', array('task' => $task, 'project' => $project)) ?>
<?php
$editable = $this->user->hasProjectAccess('CommentController', 'edit', $project['id']);
?>
<h3>Comments</h3>
<?php if (!isset($is_public) || !$is_public): ?>
    <div class="comment-sorting">
        <small>
            <?= $this->url->icon('sort', t('Change sorting'), 'CommentController', 'toggleSorting', array('task_id' => $task['id'], 'project_id' => $task['project_id'])) ?>
            <?php if ($editable): ?>
                <?= $this->modal->medium('paper-plane', t('Send by email'), 'CommentMailController', 'create', array('task_id' => $task['id'], 'project_id' => $task['project_id'])) ?>
            <?php endif ?>
        </small>
    </div>
<?php endif ?>
<?php foreach ($comments as $comment): ?>
    <?= $this->render('comment/show', array(
        'comment'   => $comment,
        'task'      => $task,
        'project'   => $project,
        'editable'  => $editable,
        'is_public' => isset($is_public) && $is_public,
    )) ?>
<?php endforeach ?>

<?php if ($editable): ?>
    <?= $this->render('task_comments/create', array(
        'values'   => array(
            'user_id' => $this->user->getId(),
            'task_id' => $task['id'],
            'project_id' => $task['project_id'],
        ),
        'errors'   => array(),
        'task'     => $task,
    )) ?>
<?php endif ?>

<?php
//Remove the title since I'm displaying that at the top
$task['title'] = "&nbsp;";
?>
<?= $this->render('task/details', array(
    'task' => $task,
    'tags' => $tags,
    'project' => $project,
    'editable' => $this->user->hasProjectAccess('TaskModificationController', 'edit', $project['id']),
)) ?>

<?= $this->hook->render('template:task:show:bottom', array('task' => $task, 'project' => $project)) ?>

</section>
//clicking on a task will open it's full view 

function clickItem(item){
	const clickObj = item;
	if (typeof clickObj.fireEvent == typeof function(){}) {
	    clickObj.fireEvent('onclick');
	  } else {
	    var evObj = document.createEvent('Events');
	    evObj.initEvent('click', true, false);
	    clickObj.dispatchEvent(evObj);
	  }
}

let items = document.querySelectorAll('.task-board');

function handleTaskClick(item, event){
  // console.log('task clicked');
  // console.log(item);
			const dropdown = item.querySelector('i.fa.fa-caret-down');
			const edit = item.querySelectorAll('i.fa-edit')[1];
			const editLink = edit.parentNode;
			const menu = item.querySelector('a.dropdown-menu.dropdown-menu-link-icon');
			const strong = menu?.querySelector('strong');
			const view = item.querySelector("a.view-task");
			if (event.target.classList.contains('fa')||event.target==view||event.target==editLink||event.target==edit||event.target==dropdown||event.target==menu||event.target==strong){
        // console.log('task view canceled');
				return;

			}
			event.preventDefault();
			event.stopPropagation();

			clickItem(view);
			setTimeout(changeCommentButtonText, 250
			);
}

for (const item of items){
	item.addEventListener('click',
		handleTaskClick.bind(this,item)
	);
  // item.addEventListener('touch',
    // handleTaskClick.bind(this,item)
  // );
}


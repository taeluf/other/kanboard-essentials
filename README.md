# Kanboard Essentials
This seeks to provide some features I find essential to using Kanboard.

## Abandoned | March 3, 2022
I do not intend to continue development. I think it still works with kanboard, but I don't know. If you fork & improve it, please submit an issue with a link and description of your fork, I'll add it here.

## Notice | Oct 18, 2021
Its not perfect. I don't work on it much, but I still think it's nice.

Feel free to submit an issue. If I don't respond, contact me @TaelufDev on twitter. If you have a better fork, submit an issue or send a merge request & I'll add it here.

## Features
I'll try to list them all here, but I may forget some:
- 'Add Column' and 'Edit Columns' buttons on Board page
- 'Complete' and 'Edit' buttons on cards
- Overview on Dashboard shows all projects in a responsive grid (or maybe up to 30? Idr)
- View task in popup instead of going to page (is this actually new? idr)
- When viewing a task, empty sections are not shown.
- edit or delete columns directly from board page

## Fixes
- Board page does not shift after loading any more. I think it was something to do with the 'Display another project' dropdown.
- Scrolling on mobile was weird. I increased the bottom padding & that helps
- Some other mobile fixes? I think. Idr

## Problems
- The bottom padding thing for mobile ruins swimlanes. They still display, they just... take up WAYY to much space.


## Screenshots:
I'm using [Blueboard](https://github.com/bgibout/blueboard) in these screenshots, which is not part of this plugin.

![Overview page](docs/overview-page.png)

![Board page](docs/board-page.png)
